package fr.maxds.configproperties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class ConfigpropertiesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigpropertiesApplication.class, args);
	}

}
