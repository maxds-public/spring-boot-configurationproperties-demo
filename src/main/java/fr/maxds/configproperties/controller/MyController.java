package fr.maxds.configproperties.controller;

import fr.maxds.configproperties.config.ConfigWithConstructorBinding;
import fr.maxds.configproperties.config.ConfigWithNestedConfig;
import fr.maxds.configproperties.config.Nested;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class MyController {

    private static final Logger log = LoggerFactory.getLogger(MyController.class);

    private final ConfigWithConstructorBinding configWithConstructorBinding;
    private final ConfigWithNestedConfig configWithNestedConfig;

    public MyController(ConfigWithConstructorBinding configWithConstructorBinding, ConfigWithNestedConfig configWithNestedConfig) {
        this.configWithConstructorBinding = configWithConstructorBinding;
        this.configWithNestedConfig = configWithNestedConfig;
    }

    @GetMapping
    public void logEverything() {
        Nested nested = configWithNestedConfig.getNested();
        log.info("ConstructorBinding : {} and {} \n Nested : {}, {} and {}", configWithConstructorBinding.getMaSuperPropriete(),
                configWithConstructorBinding.getMonAutreSuperPropriete(),
                nested.getEmail(),
                nested.getPropertyName(),
                nested.isAmMyNested());
    }
}
