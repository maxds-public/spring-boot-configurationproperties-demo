package fr.maxds.configproperties.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.*;


@Validated
@ConfigurationProperties("maxds.constructor-binding")
@ConstructorBinding
public class ConfigWithConstructorBinding {

    public ConfigWithConstructorBinding(@NotEmpty @Pattern(regexp = "^(?:M(?:AX|ax)|max)$") String maSuperPropriete, @Min(42) @Max(43) @NotNull Integer monAutreSuperPropriete) {
        this.maSuperPropriete = maSuperPropriete;
        this.monAutreSuperPropriete = monAutreSuperPropriete;
    }

    /**
     * Une propriété de Type "Super"
     */
    @NotEmpty
    @Pattern(regexp = "^(?:M(?:AX|ax)|max)$")
    private final String maSuperPropriete;

    /**
     * Une propriété de Type "Super"
     */
    @Min(42)
    @Max(43)
    @NotNull
    private final Integer monAutreSuperPropriete;

    public String getMaSuperPropriete() {
        return maSuperPropriete;
    }

    public Integer getMonAutreSuperPropriete() {
        return monAutreSuperPropriete;
    }
}

