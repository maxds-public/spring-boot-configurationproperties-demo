package fr.maxds.configproperties.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@Validated
@ConfigurationProperties("maxds")
public class ConfigWithNestedConfig {

    @NestedConfigurationProperty
    @Valid
    private Nested nested;

    public Nested getNested() {
        return nested;
    }

    public void setNested(Nested nested) {
        this.nested = nested;
    }
}
