package fr.maxds.configproperties.config;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

public class Nested {

    @AssertTrue
    private boolean amMyNested;

    @Size(min = 2, max = 12)
    private String propertyName;

    @Email
    private String email;

    public boolean isAmMyNested() {
        return amMyNested;
    }

    public void setAmMyNested(boolean amMyNested) {
        this.amMyNested = amMyNested;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
